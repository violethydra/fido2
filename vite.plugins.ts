import type { Options } from 'unplugin-auto-import/types';

export default {
  // _ViteFonts: { google: { families: ['Source Sans Pro'] } },
  _Components: {
    dts: true,
    directoryAsNamespace: false,
    useTypescript: true,
    useConfig: true,
  },
  _vueI18n: {
    include: './src/locales/**',
    compositionOnly: true,
  },
  _AutoImport: {
    include: [/\.[jt]sx?$/, /\.vue$/, /\.vue\?vue/, /\.md$/],
    imports: [
      'vue',
      'vue-router',
      'pinia',
      // 'vue-toastification',
      {
        'vue-i18n': ['useI18n'],
      },
    ],
  } as Options,
  _vitePluginImp: {
    libList: [
      {
        libName: 'ant-design-vue',
        style: (name: string) => `ant-design-vue/es/${name}/style/index.css`,
      },
    ],
  },
};
