/* eslint-env node */
/* eslint-disable unicorn/prefer-module */
// eslint-disable-next-line unicorn/prefer-module
require('@rushstack/eslint-patch/modern-module-resolution');
const rules = require('./.eslint-rules');

const arrayType = ['.js', '.mjs', '.jsx', '.ts', '.d.ts', '.tsx'];
const alias = {
  '@': './src',
  '@api': './src/api',
  '@assets': './src/assets',
  '@components': './src/components',
  '@pages': './src/pages',
  '@routers': './src/routers',
  '@store': './src/store',
  '@stores': './src/stores',
  '@utils': './src/utils',
};

module.exports = {
  rules,

  parser: 'vue-eslint-parser',
  parserOptions: {
    sourceType: 'module',
    ecmaVersion: 'latest',
    parser: '@typescript-eslint/parser',
    ecmaFeatures: { jsx: true },
  },

  env: {
    'vue/setup-compiler-macros': true,
    browser: true,
    es6: true,
    node: true,
    jest: true,
  },
  extends: [
    'eslint:recommended',
    'plugin:import/recommended',
    'plugin:import/typescript',
    // sonar
    'plugin:sonarjs/recommended',
    // vue
    'plugin:vue/base',
    'plugin:vue/vue3-recommended',
    'plugin:vue/vue3-strongly-recommended',
    'plugin:vue/vue3-essential',
    // @vue
    '@vue/eslint-config-typescript/recommended',
    '@vue/eslint-config-prettier',
  ],

  plugins: [
    'vue',
    'unicorn',
    'import',
    'package-json',
    'promise',
    'optimize-regex',

    'sonarjs',

    'prettier',
    //
  ],

  root: true,

  settings: {
    'import/parsers': { '@typescript-eslint/parser': ['.ts', '.tsx'] },

    'import/extensions': arrayType,
    'import/resolver': {
      typescript: {
        alwaysTryTypes: true,
        project: './tsconfig.json',
      },
      alias: true,

      node: { extensions: arrayType, moduleDirectory: ['src', 'node_modules'] },

      'eslint-import-resolver-custom-alias': {
        alias,
        extensions: arrayType,
      },
    },
    'prettier-vue': {
      // Settings for how to process Vue SFC Blocks
      SFCBlocks: {
        template: true,
        script: true,
        style: true,

        // Settings for how to process custom blocks
        customBlocks: {
          docs: { lang: 'markdown' },
          config: { lang: 'json' },
          module: { lang: 'js' },
          comments: false,
        },
      },
      usePrettierrc: true,

      fileInfoOptions: {
        ignorePath: '.testignore',
        withNodeModules: false,
      },
    },
  },
};
