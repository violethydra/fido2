/* eslint-disable @typescript-eslint/no-var-requires */
const { resolve } = require('path');
const rootNoExt = ({ name, type }) => resolve(process.cwd(), `src/style/${type}/`, `${name}`);

const lib = {
  'postcss-at-rules-variables': {},

  'postcss-nested': {},

  'postcss-mixins': {
    beforeEach: ['postcss-custom-properties', 'postcss-simple-vars'],
    afterEach: ['postcss-at-rules-variables', 'postcss-nested'],
    mixinsDir: rootNoExt({ name: 'mixins', type: 'pcss' }),
  },

  'postcss-for': {},
  'postcss-each': {
    afterEach: ['postcss-at-rules-variables', 'postcss-nested'],
    beforeEach: [],
  },

  'postcss-url': {},
  'postcss-import': {},

  'postcss-responsive-type': {},
  'postcss-color-mod-function': {},
  'postcss-hexrgba': {},

  'postcss-sorting': {
    'properties-order': [
      'content',
      'display',
      'background',
      'background-image',
      'background-color',
      'width',
      'height',
      'margin',
      'padding',
      'border',
      'position',
      'top',
      'right',
      'bottom',
      'left',
      'transform',
      'cursor',
      //
    ],
  },
  'postcss-pxtorem': {
    rootValue: 16,
    propList: ['*', '!border*'],
  },
  'postcss-calc': {},
  'postcss-gap-properties': {},
  'postcss-combine-duplicated-selectors': {},
  'postcss-reporter': {},
};

module.exports = {
  plugins: {
    ...lib,
    cssnano: {
      preset: [
        'default',
        {
          autoprefixer: false,
          calc: false,
          discardComments: { removeAll: true },
        },
      ],
    },
  },
  order: 'presetEnvAndCssnanoLast',
  preset: {
    stage: 2,
    autoprefixer: {
      grid: true,
      flex: true,
      overrideBrowserslist: ['last 3 versions'],
    },
  },
};
