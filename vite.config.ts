import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';

import mkcert from 'vite-plugin-mkcert';

import Components from 'unplugin-vue-components/vite';
import { ViteAliases } from 'vite-aliases';
import ViteFonts from 'vite-plugin-fonts';
import cfg from './vite.plugins';
import AutoImport from 'unplugin-auto-import/vite';
import vitePluginImp from 'vite-plugin-imp';
import PkgConfig from 'vite-plugin-package-config';
import OptimizationPersist from 'vite-plugin-optimize-persist';

const resolverComponent = { resolvers: [] };

export default defineConfig({
  plugins: [
    vue(),
    mkcert(),
    PkgConfig(),
    OptimizationPersist(),
    Components(Object.assign(cfg._Components, resolverComponent)),
    ViteAliases(),
    ViteFonts({
      google: {
        families: ['Source Sans Pro'],
      },
    }),
    AutoImport(cfg._AutoImport),
    vitePluginImp(cfg._vitePluginImp),
  ],
  server: {
    https: true,
  },
  build: {
    commonjsOptions: { transformMixedEsModules: true },
    sourcemap: true,
  },

  logLevel: 'info',
});
