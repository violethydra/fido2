import { defineStore, acceptHMRUpdate } from 'pinia';
import { PARAM, UserVerification } from '@components/Base/types';

type StateStore = Record<string, string> & { item: PARAM[] };

export const useUserStore = defineStore({
  id: 'user',
  state: () => ({
    user: {} as { user: StateStore },
    pubKeyCredParams: {} as { pubKeyCredParams: StateStore },
    authenticatorSelection: {} as { authenticatorSelection: StateStore },

    credential: {},
    credentialState: 'New',
  }),

  getters: {
    credentialLength: (state) => Object.keys(state.credential).length > 0,
    getUser: (state) => state.user?.user?.item,
    getPubKeyCred: (state) => state.pubKeyCredParams?.pubKeyCredParams?.item,
    getAuth: (state) => state.authenticatorSelection?.authenticatorSelection?.item,
  },

  actions: {
    findUserParam(property: string) {
      if (Object.keys(this.getUser).length > 0) {
        return this.getUser.find((f) => f?.label === property)?.value;
      }
      return false;
    },

    filterPubKeyCredParam() {
      return this.getPubKeyCred.filter((f) => f?.value).map(({ label, alg }) => ({ label, alg, type: 'public-key' }));
    },

    findAuthParam(property: string) {
      if (Object.keys(this.getAuth).length > 0) {
        return this.getAuth.find((f) => f?.label === property)?.value;
      }
      return false;
    },
  },
});

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useUserStore, import.meta.hot));
}
