// eslint-disable-next-line unicorn/prefer-node-protocol
import { Buffer } from 'buffer';

/**
 * [binary] convert to [Uint8Array]
 * @param binary
 * @returns
 */
export const binaryToArrayBuffer = (binary: string | unknown | any) => new Uint8Array(Buffer.from(binary, 'base64'));

/**
 * [binary] convert to [base64]
 * @param arrayBuffer
 * @returns binary
 * type BufferEncoding = 'ascii' | 'utf8' | 'utf-8' | 'utf16le' | 'ucs2' | 'ucs-2' | 'base64' | 'base64url' | 'latin1' | 'binary' | 'hex';
 */
export const binaryToBase64 = (arrayBuffer: ArrayBuffer) => Buffer.from(arrayBuffer).toString('base64');

/**
 * [base64] convert to [binary]
 * @param base64 String to write to `buf`.
 * @returns binary
 */
export const base64ToBinary = (base64: string | unknown | any) => Buffer.from(base64, 'base64').toString('binary');

export const randomID = () => Math.random().toString(36).slice(-5);
