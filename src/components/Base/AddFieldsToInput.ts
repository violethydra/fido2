import { FORM, REF_FORM, PARAM, FORM_PROPS } from './types';

export const addFields = (formItems: REF_FORM) => {
  return Object.entries(formItems).reduce((accumulator, items) => {
    const [_key, _value]: [string, FORM_PROPS] = items;

    const { prefix = 'prefixObject', item } = _value;

    const mutate = item?.map((f: PARAM) => ({
      ...f,
      id: `form__${f.label}`.replaceAll(' ', ''),
      type: f.type ?? 'text',
      option: f.option ?? [],
      value: f.type === 'checkbox' ? Boolean(f.value) : f.option?.[0] ?? f.value ?? '',
    }));

    type MARK = keyof typeof mark;
    const mark = {
      prefixObject: { before: '{', after: '}' },
      prefixArray: { before: '[', after: ']' },
      prefixNull: { before: '', after: '' },
    };

    return {
      ...accumulator,
      [_key]: {
        prefix: prefix !== 'prefixObject' ? mark?.[prefix as MARK] ?? mark.prefixNull : mark.prefixObject,
        item: mutate,
      },
    };
  }, {} as FORM);
};
