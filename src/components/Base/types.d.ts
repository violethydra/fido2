export type PARAM = {
  label: string;
  type?: string;
  option?: string[];
  value: string | boolean;
  alg?: number;
  id?: string;
};

export type FORM_PROPS = {
  prefix?: Record<string, string | any> | string | any;
  item: PARAM[];
};

export interface FORM {
  user?: FORM_PROPS;
  pubKeyCredParams?: FORM_PROPS;
  authenticatorSelection?: FORM_PROPS;
}

export type REF_FORM = FORM;

export type AuthenticatorAttachment = 'platform' | 'cross-platform';
export type UserVerification = 'preferred' | 'discouraged' | 'required';

export interface BASE64 {
  authenticatorSelection: {
    authenticatorAttachment: AuthenticatorAttachment;
    requireResidentKey: boolean;
    userVerification: UserVerification;
  };
}
