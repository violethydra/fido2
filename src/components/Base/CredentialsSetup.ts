import { useUserStore } from '@stores/user';
import { binaryToArrayBuffer, binaryToBase64 } from './Utils';

export const credentialsCreate = (hostName: string, checkedButton: boolean): PublicKeyCredentialCreationOptions => {
  const store = useUserStore();

  const base64 = {
    authenticatorSelection: checkedButton
      ? {
          authenticatorAttachment: store.findAuthParam('authenticatorAttachment'),
          requireResidentKey: store.findAuthParam('requireResidentKey'),
          userVerification: store.findAuthParam('userVerification'),
        }
      : { authenticatorAttachment: 'platform', requireResidentKey: false, userVerification: 'preferred' },
  };

  return {
    challenge: binaryToArrayBuffer('Found a problem with this page?'),
    rp: { id: hostName, name: hostName },
    user: {
      id: binaryToArrayBuffer(store.findUserParam('id')),
      name: store.findUserParam('name'),
      displayName: store.findUserParam('displayName'),
    } as PublicKeyCredentialUserEntity,
    pubKeyCredParams: store.filterPubKeyCredParam() as PublicKeyCredentialParameters[],
    authenticatorSelection: base64.authenticatorSelection as AuthenticatorSelectionCriteria,
    timeout: 60_000,
    attestation: 'direct',
  };
};
// type MutateCred = typeof result;
// type ExtMutateCred = MutateCred extends CredentialCreationOptions;

export const credentialsDecode = (data: Record<string, any> | Credential | null) => {
  console.log(`[LOG] data`, `<${typeof data}>`, data);

  const { id, rawId, response, type } = data as PublicKeyCredential & { response: AuthenticatorAttestationResponse };
  return {
    id: id,
    // btoa__rawId: bytesToB64(rawId),
    rawId: binaryToBase64(rawId),
    response: {
      AuthenticatorAttestationResponse: {
        attestationObject: binaryToBase64(response.attestationObject),
        clientDataJSON: binaryToBase64(response.clientDataJSON),
      },
    },
    type,
  };
};
// as AuthenticatorAttestationResponse
